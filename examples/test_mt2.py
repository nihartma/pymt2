#!/usr/bin/env python

from mt2 import mt2

mVisA = 10  # mass of visible object on side A.  Must be >=0.
pxA = 20  # x momentum of visible object on side A.
pyA = 30  # y momentum of visible object on side A.

mVisB = 10  # mass of visible object on side B.  Must be >=0.
pxB = -20  # x momentum of visible object on side B.
pyB = -30  # y momentum of visible object on side B.

pxMiss = -5  # x component of missing transverse momentum.
pyMiss = -5  # y component of missing transverse momentum.

chiA = 4  # hypothesised mass of invisible on side A.  Must be >=0.
chiB = 7  # hypothesised mass of invisible on side B.  Must be >=0.

desiredPrecisionOnMt2 = 0  # Must be >=0.  If 0 alg aims for machine precision.  if >0, MT2 computed to supplied absolute precision.

print(
    mt2(
        mVisA, pxA, pyA,
        mVisB, pxB, pyB,
        pxMiss, pyMiss,
        chiA, chiB,
        desiredPrecisionOnMt2
    )
)
