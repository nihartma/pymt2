import ctypes
import importlib.util
import numpy as np
from numpy.ctypeslib import ndpointer
lib_filename = importlib.util.find_spec(".libMT2", "mt2").origin
libMT2 = ctypes.cdll.LoadLibrary(lib_filename)

libMT2.mt2.restype = ctypes.c_double
libMT2.mt2.argtypes = [
    ctypes.c_double, ctypes.c_double, ctypes.c_double,
    ctypes.c_double, ctypes.c_double, ctypes.c_double,
    ctypes.c_double, ctypes.c_double,
    ctypes.c_double, ctypes.c_double,
    ctypes.c_double,
    ctypes.c_bool
]


double_array = ndpointer(dtype=ctypes.c_double, ndim=1, flags="C_CONTIGUOUS")
libMT2.fill_mt2_array.restype = None
libMT2.fill_mt2_array.argtypes = [
    ctypes.c_double, double_array, double_array,
    ctypes.c_double, double_array, double_array,
    double_array, double_array,
    double_array,
    ctypes.c_size_t,
    ctypes.c_double, ctypes.c_double,
    ctypes.c_double,
    ctypes.c_bool
]


def mt2(
    mVis1, pxVis1, pyVis1,
    mVis2, pxVis2, pyVis2,
    pxMiss, pyMiss,
    mInvis1, mInvis2,
    desiredPrecisionOnMT2=0.,
    useDeciSectionsInitially=True
):
    return libMT2.mt2(
        mVis1, pxVis1, pyVis1,
        mVis2, pxVis2, pyVis2,
        pxMiss, pyMiss,
        mInvis1, mInvis2,
        desiredPrecisionOnMT2,
        useDeciSectionsInitially
    )


def mt2_array(
    mVis1, pxVis1, pyVis1,
    mVis2, pxVis2, pyVis2,
    pxMiss, pyMiss,
    mInvis1, mInvis2,
    desiredPrecisionOnMT2=0.,
    useDeciSectionsInitially=True
):
    n_entries = len(pxVis1)
    for array in [pxVis1, pyVis1, pxVis2, pyVis2, pxMiss, pyMiss]:
        if len(array) != n_entries:
            raise ValueError("Array lengths inconsistent")
    output = np.empty(n_entries, dtype=np.float64)
    libMT2.fill_mt2_array(
        mVis1, pxVis1, pyVis1,
        mVis2, pxVis2, pyVis2,
        pxMiss, pyMiss,
        output,
        n_entries,
        mInvis1, mInvis2,
        desiredPrecisionOnMT2,
        useDeciSectionsInitially
    )
    return output
