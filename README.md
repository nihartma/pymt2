# Python interface for MT2

### Note: This is meant to be a proof-of-concept and not very well tested, so use with care
### A more sophisticated implementation can be found at https://pypi.org/project/mt2/

This uses the header-only implementation from https://www.hep.phy.cam.ac.uk/~lester/mt2/. Install via

```
git clone https://gitlab.cern.ch/nihartma/pymt2.git
cd pymt2
pip install --user ./
```

See [examples](examples) for usage.


